#!/usr/bin/env bash

alias env-up='docker-compose up -d'
alias env-stop='docker-compose stop'
alias env-exec='docker-compose exec php'
alias test-api='docker-compose exec php vendor/bin/codecept run api'