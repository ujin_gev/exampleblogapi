<?php namespace App\Tests;
//use App\Tests\ApiTester;

class DefaultRouteCest
{

    public function getDefaultRoute(ApiTester $I){
        $I->wantToTest('Default route');
        $I->sendGET('/');

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            "message" => "Welcome to Example blog API!"
        ]);
    }
}
