<?php namespace App\Tests;
//use App\Tests\ApiTester;

use App\Interfaces\UserFixtureInterface;

class UserCest implements UserFixtureInterface
{

    public function getUserToken(ApiTester $I)
    {
        $I->wantToTest('Get user JWT token');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/api/v1/auth/login', [
            'username' => SELF::USERS['user1']['username'],
            'password' => SELF::USERS['user1']['password'],
        ]);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'token' => 'string'
        ]);
    }

    public function registerNewUser(ApiTester $I)
    {
        $I->wantToTest('Registration of new user');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/api/v1/auth/register', [
            'username' => SELF::USERS['user_for_registration']['username'],
            'password' => SELF::USERS['user_for_registration']['password'],
            'email' => SELF::USERS['user_for_registration']['email'],
        ]);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'username' => 'string',
            'email' => 'string'
        ]);
        $I->seeResponseContainsJson([
            'username' => SELF::USERS['user_for_registration']['username'],
            'email' => SELF::USERS['user_for_registration']['email'],
        ]);
    }
}
