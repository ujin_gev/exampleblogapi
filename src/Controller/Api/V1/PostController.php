<?php

namespace App\Controller\Api\V1;
use App\Entity\Blog\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/blog/post")
 */
class PostController extends AbstractController
{

    /**
     * @Route("", name="api_blog_post_get_list",  methods={"GET"})
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function getListAction(){
        $em = $this->getDoctrine()->getManager();
        $postsRepository = $em->getRepository(Post::class);
        $posts = $postsRepository->findAll();
        return $this->json($posts, 200, [], ['groups'=>'default']);
    }


    /**
     * @Route("", name="api_blog_post_create",  methods={"POST"})
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $post = new Post();

        $post->setTitle($data['title'])
            ->setContent($data['content'])
            ->setUser($this->getUser());

        $em = $this->getDoctrine()->getManager();
        $em->persist($post);
        $em->flush();

        return new JsonResponse($post, 201);
    }

    /**
     * @Route("", name="api_blog_post_edit",  methods={"PUT"})
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Request $request)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $em = $this->getDoctrine()->getManager();

        $post = $em->getRepository(Post::class)->findOneBy(['slug' => $data['slug']]);
        $this->denyAccessUnlessGranted('edit', $post);
        $post->setTitle($data['title'])
            ->setContent($data['content'])
            ->setUser($this->getUser());

        $em->persist($post);
        $em->flush();

        return new JsonResponse($post, 200);
    }

}