<?php

namespace App\Interfaces;

interface UserFixtureInterface {

    public CONST USERS = [
        'user1' => [
            'username' => 'user',
            'password' => '123',
            'email' => 'test@test.com',
        ],
        'user_for_registration' => [
            'username' => 'test_registration_user',
            'password' => 'test_registration_password',
            'email' => 'test_registration@email.com',
        ]
    ];

}