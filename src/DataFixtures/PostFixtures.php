<?php

namespace App\DataFixtures;

use App\Entity\Blog\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PostFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $post1 = new Post();

        $user1 = $this->getReference(UserFixtures::USER1_REFERENCE);

        $post1
            ->setTitle('First post in blog')
            ->setContent('First content in blog')
            ->setUser($user1)
            ->setCreated(new \DateTime('2010-01-01 00:00:00'))
            ->setUpdated(new \DateTime('2010-01-01 00:00:00'))
        ;

        $manager->persist($post1);
        $manager->flush();
    }

    public static function getGroups(): array
    {
        return [
            'Post',
        ];
    }

    public function getDependencies(){
        return [
            UserFixtures::class,
        ];
    }
}
