<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Interfaces\UserFixtureInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\UserManagerInterface;

class UserFixtures extends Fixture implements FixtureGroupInterface, UserFixtureInterface
{

    public CONST USER1_REFERENCE = 'user1';

    private $userManager;

    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();

        $user
            ->setUsername(SELF::USERS['user1']['username'])
            ->setEmail(SELF::USERS['user1']['email'])
            ->setEnabled(true)
            ->setPlainPassword(SELF::USERS['user1']['password'])
        ;

        $this->userManager->updateUser($user);

        $manager->flush();

        $this->addReference(SELF::USER1_REFERENCE, $user);
    }

    public static function  getGroups(): array
    {
        return ['User'];
    }
}
